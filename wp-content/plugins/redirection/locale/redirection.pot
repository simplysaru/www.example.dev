# Copyright (C) 2017 Redirection
# This file is distributed under the same license as the Redirection package.
msgid ""
msgstr ""
"Project-Id-Version: Redirection\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Report-Msgid-Bugs-To: https://wordpress.org/plugins/redirection/\n"
"X-Poedit-Basepath: ..\n"
"X-Poedit-KeywordsList: __;_e;_ex:1,2c;_n:1,2;_n_noop:1,2;_nx:1,2,4c;_nx_noop:1,2,3c;_x:1,2c;esc_attr__;esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c\n"
"X-Poedit-SearchPath-0: .\n"
"X-Poedit-SearchPathExcluded-0: *.js\n"
"X-Poedit-SourceCharset: UTF-8\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: redirection-admin.php:159
msgid "Settings"
msgstr ""

#: redirection-admin.php:172
msgid "Log entries"
msgstr ""

#: redirection-admin.php:300
msgid "%d redirection was successfully imported"
msgid_plural "%d redirections were successfully imported"
msgstr[0] ""
msgstr[1] ""

#: redirection-admin.php:302
msgid "No items were imported"
msgstr ""

#: redirection-admin.php:313
msgid "Redirection Log"
msgstr ""

#: redirection-admin.php:319
msgid "Redirection 404"
msgstr ""

#: redirection-admin.php:325
msgid "Your group was added successfully"
msgstr ""

#: redirection-admin.php:328
msgid "Please specify a group name"
msgstr ""

#: redirection-admin.php:445
msgid "Sorry, but your redirection was not created"
msgstr ""

#: redirection-strings.php:4
msgid "No! Don't delete the logs"
msgstr ""

#: redirection-strings.php:5
msgid "Yes! Delete the logs"
msgstr ""

#: redirection-strings.php:6
msgid "Once deleted your current logs will no longer be available. You can set an delete schedule from the Redirection options if you want to do this automatically."
msgstr ""

#: redirection-strings.php:7
msgid "Delete the logs - are you sure?"
msgstr ""

#: redirection-strings.php:8
msgid "Delete All"
msgstr ""

#: redirection-strings.php:9
msgid "Export to CSV"
msgstr ""

#: redirection-strings.php:10, redirection-strings.php:15
msgid "IP"
msgstr ""

#: redirection-strings.php:11, redirection-strings.php:16, matches/referrer.php:21
msgid "Referrer"
msgstr ""

#: redirection-strings.php:12, redirection-strings.php:17, view/add.php:13, view/item-edit.php:4
msgid "Source URL"
msgstr ""

#: redirection-strings.php:13, redirection-strings.php:18
msgid "Date"
msgstr ""

#: redirection-strings.php:14, redirection-strings.php:19
msgid "Show only this IP"
msgstr ""

#: redirection-strings.php:20
msgid "Add Redirect"
msgstr ""

#: redirection-strings.php:21, redirection-strings.php:31, view/group-edit.php:22, view/item-edit.php:33
msgid "Cancel"
msgstr ""

#: redirection-strings.php:22, view/group-edit.php:21, view/item-edit.php:32
msgid "Save"
msgstr ""

#: redirection-strings.php:23
msgid "Automatically remove or add www to your site."
msgstr ""

#: redirection-strings.php:24
msgid "Add WWW"
msgstr ""

#: redirection-strings.php:25
msgid "Remove WWW"
msgstr ""

#: redirection-strings.php:26
msgid "Default server"
msgstr ""

#: redirection-strings.php:27
msgid "Canonical URL"
msgstr ""

#: redirection-strings.php:28
msgid "WordPress is installed in: {{code}}%s{{/code}}"
msgstr ""

#: redirection-strings.php:29
msgid "If you want Redirection to automatically update your {{code}}.htaccess{{/code}} file then enter the full path and filename here. You can also download the file and update it manually."
msgstr ""

#: redirection-strings.php:30
msgid ".htaccess Location"
msgstr ""

#: redirection-strings.php:32
msgid "Download"
msgstr ""

#: redirection-strings.php:33
msgid "Failed to load"
msgstr ""

#: redirection-strings.php:34, models/pager.php:244, models/pager.php:438, view/submenu.php:6
msgid "Redirects"
msgstr ""

#: redirection-strings.php:35, models/pager.php:245, models/pager.php:437, view/group-edit.php:8
msgid "Module"
msgstr ""

#: redirection-strings.php:36, models/pager.php:448
msgid "Configure"
msgstr ""

#: redirection-strings.php:37
msgid "For use with Nginx server. Requires manual configuration. The redirect happens without loading WordPress. No tracking of hits. This is an experimental module."
msgstr ""

#: redirection-strings.php:38
msgid "Uses Apache {{code}}.htaccess{{/code}} files. Requires further configuration. The redirect happens without loading WordPress. No tracking of hits."
msgstr ""

#: redirection-strings.php:39
msgid "WordPress-powered redirects. This requires no further configuration, and you can track hits."
msgstr ""

#: redirection-strings.php:40
msgid "No! Don't delete the plugin"
msgstr ""

#: redirection-strings.php:41
msgid "Yes! Delete the plugin"
msgstr ""

#: redirection-strings.php:42
msgid "Once deleted your redirections will stop working. If they appear to continue working then please clear your browser cache."
msgstr ""

#: redirection-strings.php:43
msgid "Deleting the plugin will remove all your redirections, logs, and settings. Do this if you want to remove the plugin for good, or if you want to reset the plugin."
msgstr ""

#: redirection-strings.php:44
msgid "Delete the plugin - are you sure?"
msgstr ""

#: redirection-strings.php:45, redirection-strings.php:106, models/pager.php:54, models/pager.php:101, models/pager.php:254, models/pager.php:300
msgid "Delete"
msgstr ""

#: redirection-strings.php:46
msgid "Delete Redirection"
msgstr ""

#: redirection-strings.php:47
msgid "Source URL, Target URL, [Regex 0=false, 1=true], [HTTP Code]"
msgstr ""

#: redirection-strings.php:48
msgid "CSV Format"
msgstr ""

#: redirection-strings.php:49
msgid "Upload"
msgstr ""

#: redirection-strings.php:50
msgid "Here you can import redirections from an existing {{code}}.htaccess{{/code}} file, or a CSV file."
msgstr ""

#: redirection-strings.php:51
msgid "Import"
msgstr ""

#: redirection-strings.php:52
msgid "Failed to load data"
msgstr ""

#: redirection-strings.php:53
msgid "Update"
msgstr ""

#: redirection-strings.php:54
msgid "This will be used to auto-generate a URL if no URL is given. You can use the special tags {{code}}$dec${{/code}} or {{code}}$hex${{/code}} to have a unique ID inserted (either decimal or hex)"
msgstr ""

#: redirection-strings.php:55
msgid "Auto-generate URL"
msgstr ""

#: redirection-strings.php:56
msgid "A unique token allowing feed readers access to Redirection log RSS (leave blank to auto-generate)"
msgstr ""

#: redirection-strings.php:57
msgid "RSS Token"
msgstr ""

#: redirection-strings.php:58
msgid "Monitor changes to posts"
msgstr ""

#: redirection-strings.php:59, redirection-strings.php:61
msgid "(time to keep logs for)"
msgstr ""

#: redirection-strings.php:60
msgid "404 Logs"
msgstr ""

#: redirection-strings.php:62
msgid "Redirect Logs"
msgstr ""

#: redirection-strings.php:63
msgid "I'm a nice person and I have helped support the author of this plugin"
msgstr ""

#: redirection-strings.php:64
msgid "Plugin support"
msgstr ""

#: redirection-strings.php:65
msgid "Failed to save data"
msgstr ""

#: redirection-strings.php:66
msgid "Your options were updated"
msgstr ""

#: redirection-strings.php:67
msgid "Don't monitor"
msgstr ""

#: redirection-strings.php:68
msgid "Forever"
msgstr ""

#: redirection-strings.php:69
msgid "Two months"
msgstr ""

#: redirection-strings.php:70
msgid "A month"
msgstr ""

#: redirection-strings.php:71
msgid "A week"
msgstr ""

#: redirection-strings.php:72
msgid "A day"
msgstr ""

#: redirection-strings.php:73
msgid "No logs"
msgstr ""

#: redirection-strings.php:74
msgid "Thank you for making a donation!"
msgstr ""

#: redirection-strings.php:75
msgid "Yes I'd like to donate"
msgstr ""

#: redirection-strings.php:76
msgid "Please note I do not provide support and this is just a donation."
msgstr ""

#: redirection-strings.php:77
msgid "You get some useful software and I get to carry on making it better."
msgstr ""

#: redirection-strings.php:78
msgid "Redirection is free to use - life is wonderful and lovely! It has required a great deal of time and effort to develop and you can help support this development by {{strong}}making a small donation{{/strong}}."
msgstr ""

#: redirection-strings.php:79
msgid "I'd like to donate some more"
msgstr ""

#: redirection-strings.php:80
msgid "You've already supported this plugin - thank you!"
msgstr ""

#: redirection-strings.php:81
msgid "Need some help? Maybe one of these questions will provide an answer"
msgstr ""

#: redirection-strings.php:82
msgid "Frequently Asked Questions"
msgstr ""

#: redirection-strings.php:83
msgid "Please disable all other plugins and check if the problem persists. If it does please report it {{a}}here{{/a}} with full details about the problem and a way to reproduce it."
msgstr ""

#: redirection-strings.php:84
msgid "Something isn't working!"
msgstr ""

#: redirection-strings.php:85
msgid "It's not possible to do this on the server. Instead you will need to add {{code}}target=\"blank\"{{/code}} to your link."
msgstr ""

#: redirection-strings.php:86
msgid "Can I open a redirect in a new tab?"
msgstr ""

#: redirection-strings.php:87
msgid "Your browser will cache redirections. If you have deleted a redirection and your browser is still performing the redirection then {{a}}clear your browser cache{{/a}}."
msgstr ""

#: redirection-strings.php:88
msgid "I deleted a redirection, why is it still redirecting?"
msgstr ""

#: redirection-strings.php:89
msgid "Your email address:"
msgstr ""

#: redirection-strings.php:90
msgid "Sign up for the tiny Redirection newsletter - a low volume newsletter about new features and changes to the plugin. Ideal if want to test beta changes before release."
msgstr ""

#: redirection-strings.php:91
msgid "Want to keep up to date with changes to Redirection?"
msgstr ""

#: redirection-strings.php:92, redirection-strings.php:94
msgid "Newsletter"
msgstr ""

#: redirection-strings.php:93
msgid "Thanks for subscribing! {{a}}Click here{{/a}} if you need to return to your subscription."
msgstr ""

#: redirection-strings.php:95
msgid "No results"
msgstr ""

#: redirection-strings.php:96
msgid "Sorry but something went wrong loading the data - please try again"
msgstr ""

#: redirection-strings.php:97
msgid "Select All"
msgstr ""

#: redirection-strings.php:98
msgid "%s item"
msgid_plural "%s items"
msgstr[0] ""
msgstr[1] ""

#: redirection-strings.php:99
msgid "Last page"
msgstr ""

#: redirection-strings.php:100
msgid "Next page"
msgstr ""

#: redirection-strings.php:101
msgid "of %(page)s"
msgstr ""

#: redirection-strings.php:102
msgid "Current Page"
msgstr ""

#: redirection-strings.php:103
msgid "Prev page"
msgstr ""

#: redirection-strings.php:104
msgid "First page"
msgstr ""

#: redirection-strings.php:105
msgid "Apply"
msgstr ""

#: redirection-strings.php:107
msgid "Bulk Actions"
msgstr ""

#: redirection-strings.php:108
msgid "Select bulk action"
msgstr ""

#: redirection-strings.php:109
msgid "Search"
msgstr ""

#: redirection-strings.php:110
msgid "Search by IP"
msgstr ""

#: matches/login.php:7
msgid "URL and login status"
msgstr ""

#: matches/login.php:16
msgid "The target URL will be chosen from one of the following URLs, depending if the user is logged in or out.  Leaving a URL blank means that the user is not redirected."
msgstr ""

#: matches/login.php:23, matches/login.php:25
msgid "Logged In"
msgstr ""

#: matches/login.php:35, matches/login.php:37
msgid "Logged Out"
msgstr ""

#: matches/referrer.php:8
msgid "URL and referrer"
msgstr ""

#: matches/referrer.php:24, view/item-edit.php:7
msgid "Regex"
msgstr ""

#: matches/referrer.php:28, matches/referrer.php:38, matches/url.php:20, matches/user-agent.php:38
msgid "HTTP Code"
msgstr ""

#: matches/referrer.php:40
msgid "The visitor will be redirected from the source URL if the referrer matches.  You can specify a <em>matched</em> target URL as the address to send visitors if they do match, and <em>not matched</em> if they don't match.  Leaving a URL blank means that the visitor is not redirected."
msgstr ""

#: matches/referrer.php:46, matches/referrer.php:48, matches/user-agent.php:58, matches/user-agent.php:60
msgid "Matched"
msgstr ""

#: matches/referrer.php:56, matches/referrer.php:58, matches/user-agent.php:68, matches/user-agent.php:70
msgid "Not matched"
msgstr ""

#: matches/url.php:5
msgid "URL only"
msgstr ""

#: matches/url.php:12, view/add.php:32
msgid "Target URL"
msgstr ""

#: matches/user-agent.php:7
msgid "URL and user agent"
msgstr ""

#: matches/user-agent.php:12
msgid "FeedBurner"
msgstr ""

#: matches/user-agent.php:13
msgid "Internet Explorer"
msgstr ""

#: matches/user-agent.php:14
msgid "FireFox"
msgstr ""

#: matches/user-agent.php:15
msgid "Opera"
msgstr ""

#: matches/user-agent.php:16
msgid "Safari"
msgstr ""

#: matches/user-agent.php:17
msgid "iPhone"
msgstr ""

#: matches/user-agent.php:18
msgid "iPad"
msgstr ""

#: matches/user-agent.php:19
msgid "Android"
msgstr ""

#: matches/user-agent.php:20
msgid "Nintendo Wii"
msgstr ""

#: matches/user-agent.php:25
msgid "User Agent"
msgstr ""

#: matches/user-agent.php:51
msgid ""
"The visitor will be redirected from the source URL if the user agent matches.  You can specify a <em>matched</em> target URL as the address to send visitors if they do match, and <em>not matched</em> if they don't match.  Leaving a URL blank means that the visitor is not redirected. <strong>All matches are performed as regular expressions</strong>.\n"
""
msgstr ""

#: models/database.php:120, view/item-list.php:3
msgid "Redirections"
msgstr ""

#: models/database.php:121
msgid "Modified Posts"
msgstr ""

#: models/pager.php:28
msgid "Type"
msgstr ""

#: models/pager.php:29
msgid "URL"
msgstr ""

#: models/pager.php:30
msgid "Hits"
msgstr ""

#: models/pager.php:31
msgid "Last Access"
msgstr ""

#: models/pager.php:53, models/pager.php:253
msgid "Edit"
msgstr ""

#: models/pager.php:59, models/pager.php:103, models/pager.php:260, models/pager.php:302
msgid "Disable"
msgstr ""

#: models/pager.php:61, models/pager.php:102, models/pager.php:262, models/pager.php:301
msgid "Enable"
msgstr ""

#: models/pager.php:104
msgid "Reset Hits"
msgstr ""

#: models/pager.php:151
msgid "No group filter"
msgstr ""

#: models/pager.php:243, view/group-edit.php:4, view/group-list.php:28
msgid "Name"
msgstr ""

#: models/pager.php:255
msgid "View Redirects"
msgstr ""

#: models/pager.php:279
msgid "Unknown"
msgstr ""

#: models/pager.php:356
msgid "All modules"
msgstr ""

#: models/redirect.php:160
msgid "Source and target URL must be different"
msgstr ""

#: models/redirect.php:166
msgid "You can only redirect from a relative URL (<code>%s</code>) on this domain (<code>%s</code>)."
msgstr ""

#: models/redirect.php:174
msgid "Invalid group when creating redirect"
msgstr ""

#: models/redirect.php:177
msgid "Invalid source URL when creating redirect for given match type"
msgstr ""

#: models/redirect.php:376
msgid "Redirect to URL"
msgstr ""

#: models/redirect.php:377
msgid "Redirect to random post"
msgstr ""

#: models/redirect.php:378
msgid "Pass-through"
msgstr ""

#: models/redirect.php:379
msgid "Error (404)"
msgstr ""

#: models/redirect.php:380
msgid "Do nothing"
msgstr ""

#: view/add.php:4
msgid "Add new redirection"
msgstr ""

#: view/add.php:7
msgid "Your redirection has been added."
msgstr ""

#: view/add.php:17
msgid "Match"
msgstr ""

#: view/add.php:23
msgid "Action"
msgstr ""

#: view/add.php:28
msgid "Regular expression"
msgstr ""

#: view/add.php:36, view/item-edit.php:18
msgid "Group"
msgstr ""

#: view/add.php:46
msgid "Add Redirection"
msgstr ""

#: view/group-list.php:3, view/submenu.php:11
msgid "Groups"
msgstr ""

#: view/group-list.php:20
msgid "Add Group"
msgstr ""

#: view/group-list.php:22
msgid "Use groups to organise your redirects. Groups are assigned to a module, which affects how the redirects in that group work. If you are unsure then stick to the WordPress module."
msgstr ""

#: view/group-list.php:42
msgid "Add"
msgstr ""

#: view/item-edit.php:11
msgid "Description"
msgstr ""

#: view/item-edit.php:14
msgid "optional"
msgstr ""

#: view/module-list.php:3, view/submenu.php:16
msgid "Modules"
msgstr ""

#: view/options.php:4, view/submenu.php:42
msgid "Options"
msgstr ""

#: view/submenu.php:23
msgid "Log"
msgstr ""

#: view/submenu.php:32
msgid "404s from %s"
msgstr ""

#: view/submenu.php:34
msgid "404s"
msgstr ""

#: view/submenu.php:47
msgid "Support"
msgstr ""

#: view/support.php:3
msgid "Redirection Support"
msgstr ""
